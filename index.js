/*
// the following should be sourced from config.js before the index.js is loaded
const config = {
  words: [
    {
      full: "Der Chef hat nicht gezahlt.",
      audio: "",
      segments: [ "Der", "Chef", "hat", "nicht", "gezahlt", "." ]
    },
    {
      full: "Dann hat er gezahlt – 12.600 Euro.",
      audio: "",
      segments: [ "Dann", "hat", "er", "gezahlt", "-", "12.600", "Euro", "." ]
    },
    // and so on
  ],
  // link to the next exercise
  next: "#this-link-does-not-lead-anywhere-yet",
  // directory where audio files for words and segments are stored
  audioSource: "../audio/"
}
 */

// set config and $ as global vars for the linter
/* global config */
/* global $ */

// the following two variables are to be compatible with the final WP plugin implementation
const DigMitConfig = config
const digMitExerciseID = 1

class LinguaPuzzle {
  constructor (id, config) {
    this.config = config
    this.id = id
    this.$container = $('#digmit-exercise-container-' + id)
  }

  init () {
    this.initialiseContainer()
    $('title').text(this.config.title)
    $('.header h1').text(this.config.title)
    $('.description').text(this.config.description)
    this.initialiseSegments()
    // initialiseTargets( config )
    $('#next-word-button').on('click', { self: this }, this.prepareNextWord)
    $('#next-word-button span').text(this.config.nextWordLabel)
    $('#next-exercise-button span').text(this.config.nextLabel)
    $('#next-exercise-button').on('click', () => {
      window.location = this.config.next
    })
  }

  initialiseContainer () {
    $('<div class="header"><h1></h1></div>').appendTo(this.$container)
    $('<div class="description"></div>').appendTo(this.$container)
    $('<ul class="sort-field"></ul>').appendTo(this.$container)

    const elSuccessMessage = `
      <div class="success-message" style="display: none">
        <p>
          <i class="fa fa-star fa-spin"></i>
          <i class="fa fa-star fa-spin"></i>
          <i class="fa fa-star fa-spin"></i>
          Super!
          <i class="fa fa-star fa-spin"></i>
          <i class="fa fa-star fa-spin"></i>
          <i class="fa fa-star fa-spin"></i>
        </p>
        <div id="next-exercise-button">
          <span></span>
          <i class="fa fa-arrow-circle-right"></i>
        </div>
      </div>
    `
    $(elSuccessMessage).appendTo(this.$container)

    const elNextWordMessage = `
      <div class="next-word-message" style="display: none">
        <p>
          <i class="fa fa-star fa-spin"></i>
          Super!
          <i class="fa fa-star fa-spin"></i>
        </p>
        <div id="next-word-button">
          <span></span>
          <i class="fa fa-arrow-circle-right"></i>
        </div>
      </div>
    `
    $(elNextWordMessage).appendTo(this.$container)
  }

  // take the next word from config.words array, create elements for the segments
  // and place them in the source area
  initialiseSegments () {
    const config = this.config
    // check if this is the frist word in the exercise and create counter
    // otherwise increase counter for the next word
    if (config.counter === undefined) {
      config.counter = 0
    } else {
      config.counter++
    }

    // shuffle the segments and make sure the first one is not the first of the word
    let unshuffled = config.words[config.counter].segments
    // make a copy to not manipulate the original array
    unshuffled = unshuffled.slice(0, unshuffled.length)
    const shuffled = []
    while (unshuffled.length > 0) {
      const i = Math.floor(Math.random() * unshuffled.length)
      // an index of 0 is only ok, if we're not filling the first segment
      if (shuffled.length === 0 && i === 0) {
        continue
      }
      shuffled.push(unshuffled.splice(i, 1)[0])
    }

    // first add a non-draggable audio button for the whole word
    let $el = this.createFullWord(config.words[config.counter], config.audioSource)
    $el.insertAfter($('.description'))

    // add the test button
    const $testButton = $('<button id="test-button"><i class="fa fa-check"></i></button>')
    $testButton.on('click', { self: this }, this.allCorrect)
    $testButton.insertAfter($('.sort-field'))

    for (let i in shuffled) {
      $el = $('<li class="audio-button" id="segment-' + i + '">' + shuffled[i] + '<br></li>')
      $el.appendTo($('.sort-field'))
      i++
    }
    $('.sort-field').sortable({
      start: function (e, ui) {
        ui.placeholder.height(ui.item.height())
        ui.placeholder.css('visibility', 'visible')
      },
    })
    $('.sort-field').disableSelection()
  }

  // play an audio file associated with a label
  playAudio (event) {
    const source = event.data.source
    // create label for the default file name
    const label = event.data.label.replace(/(\s)/g, '_').replace(/[^\w-]/g, '')
    // if there is an override file in the config file choose that one
    const filename = event.data.audio ? event.data.audio : label + '.mp3'
    const audio = new Audio(source + filename)
    audio.play()
  }

  // creates and returns a clickable and non-draggable button with a text
  // that is played back when the user clicks on it
  createFullWord (text, source) {
    const el = '<div class="full-word"><i class="fa fa-volume-up"></i></div>'
    const $el = $(el)
    $el.on('click', { label: text.full, audio: text.audio, source }, this.playAudio)
    return $el
  }

  allCorrect (event) {
    const config = event.data.self.config
    let correct = true
    const sourceField = $('.sort-field').first().children()
    for (const i in config.words[config.counter].segments) {
      if (sourceField.eq(i).text() !== config.words[config.counter].segments[i]) {
        correct = false
        // make button effect
        $('#test-button').addClass('test-button-effect').html('<i class="fa fa-times"></i>')
        setTimeout(() => { $('#test-button').removeClass('test-button-effect').html('<i class="fa fa-check"></i>') }, 820)
        break
      }
    }
    if (correct) {
      // add exercise-solved class
      $('.audio-button').addClass('exercise-solved')
      // create result field and move the audio button for the whole word into the target area
      $('.sort-field').remove()
      $('#test-button').remove()
      const $resultField = $('<div class="result-field">' + config.words[config.counter].full + '</div>')
      $resultField.insertAfter($('.next-word-message'))
      $('.full-word').appendTo($('.result-field'))

      // now check if this was already the last word in the exercise
      if (config.counter + 1 >= config.words.length) {
        $('.success-message').css('display', 'block')
        $('.success-message a').attr('href', config.next)
      } else {
        $('.next-word-message').css('display', 'block')
      }
    } else {
      $('.success-message').css('display', 'none')
    }
  }

  // this is called after a word was solved to prepare for the next word in the
  // exercise. we have to restore source and target fields accordingly
  prepareNextWord (event) {
    const self = event.data.self
    // make the next word messsage invisible again
    $('.next-word-message').css('display', 'none')
    // let's also remove all elements from the target field
    $('.result-field').remove()
    // and insert a new sort field
    $('<ul class="sort-field"></ul>').insertAfter($('.description'))
    // now let's re-initialise
    self.initialiseSegments()
    // initialiseTargets( config )
  }
}

// start initialisation as soon as the document is ready
$('document').ready(function () {
  const exercise = new LinguaPuzzle(digMitExerciseID, DigMitConfig)
  exercise.init()
})
