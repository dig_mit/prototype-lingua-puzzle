/* eslint no-unused-vars: ["error", { "varsIgnorePattern": "config" }] */

// config object with list of available words for this exercise
// in the integrated version this will be generated dynamically by the backend
const config = {
  words: [
    {
      full: 'Der Chef hat nicht gezahlt.',
      audio: '',
      segments: ['Der', 'Chef', 'hat', 'nicht', 'gezahlt', '.'],
    },
    {
      full: 'Dann hat er gezahlt – 12.600 Euro.',
      audio: '',
      segments: ['Dann', 'hat', 'er', 'gezahlt', '-', '12.600', 'Euro', '.'],
    },
    {
      full: 'Erntehilfe-Arbeit in Tirol ist eine sehr schwere Arbeit und sehr wenig bezahlt.',
      audio: '',
      segments: ['Erntehilfe-Arbeit', 'in', 'Tirol', 'ist', 'eine', 'sehr', 'schwere', 'Arbeit', 'und', 'sehr', 'wenig', 'bezahlt', '.'],
    },
    {
      full: 'Mein Name ist Andrei Oancea.',
      audio: '',
      segments: ['Mein', 'Name', 'ist', 'Andrei', 'Oancea', '.'],
    },
    {
      full: 'Ich und mein Bruder waren nicht zufrieden.',
      audio: '',
      segments: ['Ich', 'und', 'mein', 'Bruder', 'waren', 'nicht', 'zufrieden', '.'],
    },
  ],

  // the title / header of the exercise
  title: 'Lingua-Puzzle',
  // a sentence or short paragraph to describe the exercise
  description: 'Höre dir den Satz an und sortier die Wörter in die richtige Reihenfolge.',
  // link to the next exercise / page
  next: '#this-link-does-not-lead-anywhere-yet',
  // labels that are shown as links to the next word / exercise (or page)
  nextWordLabel: 'Nächster Satz',
  nextLabel: 'Nächste Übung',
  // directory where audio files for words and segments are stored
  audioSource: '../audio/',
}
